import React, { useState } from 'react';

class Ex6Client extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            // other stuff
            ws: null,
            wsOpen: false,
            logs: [],
            serverMessages: []
        };
        this.createWebSocket = this.createWebSocket.bind(this);
        this.closeWebSocket = this.closeWebSocket.bind(this);
        this.clearServerMessages = this.clearServerMessages.bind(this);
        this.logInHtml = this.logInHtml.bind(this);
        this.messageInHtml = this.messageInHtml.bind(this);
        this.notificationClass = this.notificationClass.bind(this);
        this.heartbeat = this.heartbeat.bind(this);
    }

    componentWillUnmount() {
        if (this.state.ws) {
            this.state.ws.onclose = null;
            this.closeWebSocket();
        }
    }

    logInHtml(text) {
        this.props.logInHtml(this.state.index, text);
    }

    messageInHtml(text) {
        const textInHtml = <pre style={{padding: "1px 5px", margin: "5px"}}>{text}</pre>;
        this.setState(prevState => ({
            serverMessages: [textInHtml, ...prevState.serverMessages]
        }))
    }

    createWebSocket () {
        const me = this;
        this.logInHtml('🧦 Tentative de connexion à la WebSocket');
        this.state.ws = new WebSocket('ws://localhost:8080/?id='+this.state.index);

        this.state.ws.onerror = function() {
            me.logInHtml('❌ Erreur WebSocket');
        }

        this.state.ws.onmessage = function(e) {
            console.log("📨 Récéption d'un message du serveur : 📜 '" + e.data + "'");
            me.messageInHtml("📨📜 " + e.data);

            if (e.data === "ping") {
                me.heartbeat();
            }
        };

        this.state.ws.onclose = function (event) {
            let message = 'suite à une erreur';
            if (event.code === 1000) {
                message = 'avec succès';
            }
            me.logInHtml(`${event.code} : Connexion WebSocket fermée ${message}`);
            me.setState({
                ws: null,
                wsOpen: false
            })
        }

        this.state.ws.onopen = function() {
            console.log('🔌 Je suis connecté');
            me.logInHtml('🔌 Je suis connecté');
            me.setState({
                wsOpen: true
            });
            me.heartbeat();
        };
    }
    
    closeWebSocket() {
        clearTimeout(this.pingTimeout);
        if (this.state.ws) {
            this.state.ws.close(1000);
        }
    }

    clearServerMessages () {
        this.setState({
            serverMessages: []
        })
    }

    notificationClass () {
        switch (this.state.index) {
            case "1":
                return "is-info";
            case "2":
                return "is-warning"
            case "3":
                return "is-success"
            case "4":
            default:
                return "is-link"
        }
    }

    heartbeat() {
        const me = this;
        console.log("❤️ J'envoie pong");
        this.logInHtml("❤️ J'envoie pong");
        this.messageInHtml("❤️ J'envoie pong");
        this.state.ws.send('pong');
        clearTimeout(this.pingTimeout);

        this.pingTimeout = setTimeout(() => {
            me.messageInHtml('💔 Ping non reçu, je me déconnecte');
            me.state.ws.close(1000);
        }, 5000 + 1000);
    }

    render() {
        return (
            <div className="tile is-parent">
                <div className={`tile notification is-light ${this.props.notificationClass(this.state.index)}`}>
                    <article className="tile is-child is-4">
                        <p className="subtitle">Client {this.state.index} 🧦</p>
                        <div className="field">
                            <div className="control">
                                <button className="button is-normal" disabled={this.state.ws} onClick={this.createWebSocket}>Se connecter</button>
                                <span className="subtitle" hidden={!this.state.ws || !this.state.wsOpen} style={{lineHeight: "40px"}}> 🔌</span>
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button className="button is-normal" disabled={!this.state.ws || !this.state.wsOpen} onClick={this.closeWebSocket}>Se déconnecter</button>
                            </div>
                        </div>
                    </article>
                    <article className="tile is-child is-8">
                        <h1 style={{lineHeight: "30px", paddingBottom: "11px"}}>Messages du server&nbsp;&nbsp;&nbsp;<button className="button is-small" onClick={this.clearServerMessages}>Clear</button></h1>
                        <div className="box" style={{height: "100px", overflow: "hidden visible", padding: "5px"}}>
                            <div>{this.state.serverMessages}</div>
                        </div>
                    </article>
                </div>
            </div>
            );
    }
}

export default Ex6Client;
import React, { useState } from 'react';
import Ex3Client from './Ex3Client';

function Ex3() {

    const [logs, updateLogs] = useState([]);

    // logs
    const logInHtml = (indexClient, text) => {
        updateLogs(logs => {
            const textInHtml = <pre 
                                style={{padding: "1px 5px", margin: "5px"}} 
                                className={`tile notification is-light ${notificationClass(indexClient)}`}>
                                    Client {indexClient} : {text}
                            </pre>;
            return [textInHtml, ...logs];
        });
    }
    const clearLogs = () => {
        updateLogs([]);
    }

    const notificationClass = (index) => {
        switch (index) {
            case "1":
                return "is-info";
            case "2":
                return "is-warning"
            case "3":
                return "is-success"
            case "4":
            default:
                return "is-link"
        }
    }

    // render
    return (
        <div className="container">
            <section className="section">
                <h1 className="title">Troisième exercice - Multi-clients 🎮</h1>
                <h2 className="subtitle">
                    Votre serveur doit diffuser un message à chacun de ses clients à chaque connexion et déconnexion.
                </h2>
                <div className="tile is-ancestor">
                    <Ex3Client index="1" notificationClass={notificationClass} logInHtml={logInHtml} />
                    <Ex3Client index="2" notificationClass={notificationClass} logInHtml={logInHtml} />
                </div>
                <div className="tile is-ancestor">
                    <Ex3Client index="3" notificationClass={notificationClass} logInHtml={logInHtml} />
                    <Ex3Client index="4" notificationClass={notificationClass} logInHtml={logInHtml} />
                </div>
            </section>
            <section className="section">
                <h2 className="title is-4">Logs&nbsp;&nbsp;<button className="button is-small" onClick={clearLogs}>Clear</button></h2>
                <div className="box">
                    <div>{logs}</div>
                </div>
            </section>
        </div>
    );
}

export default Ex3;
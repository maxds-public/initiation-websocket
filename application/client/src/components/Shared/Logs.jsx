function Logs({ clearLogs, logs = [] }) {

    return (
        <section className="section">
                <h2 className="title is-4">Logs&nbsp;&nbsp;<button className="button is-small" onClick={clearLogs}>Clear</button></h2>
                <div className="box">
                    <div>
                    {logs.map((log, index) => 
                        <pre key={index} style={{padding: "1px 5px", margin: "5px"}}>{log}</pre>
                    )}
                    </div>
                </div>
            </section>
    );
}

export default Logs;
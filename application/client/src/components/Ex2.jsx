import { useState, useEffect, useRef } from 'react';
import Logs from './Shared/Logs';

function Ex2() {

    const ws = useRef(null);
    const [message, setMessage] = useState('');
    const [logs, updateLogs] = useState([]);

    useEffect(() => {
        // Anything in here is fired on component mount.
        createWebSocket();
        return () => {
            // Anything in here is fired on component unmount.
            if (ws.current) {
                ws.current.onclose = null;
                closeWebSocket();
            }
        }
    }, []);


    // logs
    const logInHtml = (text) => {
        updateLogs(logs => [text, ...logs]);
    }
    const clearLogs = () => {
        updateLogs([]);
    }

    const createWebSocket = () => {
        logInHtml('🧦 Tentative de connexion à la WebSocket');
        ws.current = new WebSocket('ws://localhost:8080/');

        ws.current.onerror = function() {
            logInHtml('❌ Erreur WebSocket');
        }

        ws.current.onclose = (event) => {
            ws.current = null;
            if (event.code === 1000) {
                logInHtml(`${event.code} : Connexion WebSocket fermée avec succès`);
            } else {
                logInHtml(`${event.code} : Connexion WebSocket fermée suite à une erreur`);
                createWebSocket();
            }
        }

        ws.current.onopen = function() {
            logInHtml('🔌 Je suis connecté');
        };

        ws.current.onmessage = function(e) {
            logInHtml("📨 Récéption d'un message du serveur : 📜 '" + e.data + "'");
        };
    }
    
    const closeWebSocket = () => {
        if (ws.current) {
            ws.current.close(1000);
        }
    }

    const handleMessageChange = (event) => {
        setMessage(event.target.value);
    }

    const sendMessage = () => {
        if (message) {
            logInHtml(`🏌🏻💨✉️ Envoi d'un message au serveur : '${message}'`);
            ws.current.send(message);
            setMessage('');
        }
    }

    const handleKeyPress = (event) => {
        if( event.key === 'Enter' ){
            sendMessage()
        }
    }

    return (
        <div className="container" onKeyPress={handleKeyPress}>
            <section className="section">
                <h1 className="title">Deuxième exercice - Recevoir et envoyer des messages ✉️</h1>
                <h2 className="subtitle">
                    Ce client permet d'envoyer des messages, votre serveur doit être capable de les récéptionner et de renvoyer un message en retour.
                </h2>
                <div className="field">
                    <div className="control">
                        <input className="input is-normal" type="text" placeholder="Message"
                            value={message} onChange={handleMessageChange} />
                    </div>
                </div>
                <div className="field">
                    <div className="control">
                        <button className="button is-normal" disabled={!message} onClick={sendMessage}>🏌🏻💨✉️ Envoyer message</button>
                    </div>
                </div>
            </section>
            <Logs clearLogs={clearLogs} logs={logs} />
        </div>
    );
}


export default Ex2;
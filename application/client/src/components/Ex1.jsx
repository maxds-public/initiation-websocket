import { useState, useEffect, useRef } from 'react';
import Logs from './Shared/Logs';

function Ex1() {

    // validations stuff
    // TODO: gérer ça dans l'App.js en global avec une barre de progression
    // TODO: faire le closeOk dans l'exercice avec multiple client (impossible de vérifier sinon..)
    const [wsConnecting, setWsConnecting] = useState(false);
    const ws = useRef(null);
    const [wsOpen, setWsOpen] = useState(false);
    const [logs, updateLogs] = useState([]);

    useEffect(() => {
        // Anything in here is fired on component mount.
        return () => {
            // Anything in here is fired on component unmount.
            if (ws.current) {
                ws.current.onclose = null;
                closeWebSocket();
            }
        }
    }, []);

    // logs
    const logInHtml = (text) => {
        updateLogs(logs => [text, ...logs]);
    }
    const clearLogs = () => {
        updateLogs([]);
    }

    // ws
    const createWebSocket = () => {
        logInHtml('🧦 Tentative de connexion à la WebSocket');
        ws.current = new WebSocket('ws://localhost:8080/');
        setWsConnecting(true);

        ws.current.onerror = () => {
            logInHtml('❌ Erreur WebSocket');
        }

        ws.current.onclose = (event) => {
            let message = 'suite à une erreur';
            if (event.code === 1000) {
                message = 'avec succès';
            }
            logInHtml(`${event.code} : Connexion WebSocket fermée ${message}`);
            ws.current = null;
            setWsConnecting(false);
            setWsOpen(false);
        }

        ws.current.onopen = () => {
            logInHtml('🔌 Je suis connecté');
            setWsOpen(true);
        };
    }
    
    const closeWebSocket = () => {
        if (ws.current) {
            ws.current.close(1000);
        }
    }

    // render
    return (
        <div className="container">
            <section className="section">
                <h1 className="title">Premier exercice - Une poignée de main 🤝</h1>
                        
                <h2 className="subtitle">
                    Créer un serveur permettant d'accepter une connexion WebSocket sur le port 8080.
                </h2>
                <div className="field">
                    <div className="control">
                        <button className="button is-normal" disabled={ws.current || wsConnecting} onClick={createWebSocket}>Se connecter</button>
                    </div>
                </div>
                <div className="field">
                    <div className="control">
                        <button className="button is-normal" disabled={!ws.current || !wsOpen} onClick={closeWebSocket}>Se déconnecter</button>
                    </div>
                </div>
            </section>
            <Logs clearLogs={clearLogs} logs={logs} />
        </div>
    );
}

export default Ex1;
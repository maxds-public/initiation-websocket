import React, { useState } from 'react';
import Ex4Client from './Ex4Client';

class Ex4 extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            logs: []
        };
        this.logInHtml = this.logInHtml.bind(this);
        this.clearLogs = this.clearLogs.bind(this);
    }

    logInHtml(indexClient, text) {
        const textInHtml = <pre 
                                style={{padding: "1px 5px", margin: "5px"}} 
                                className={`tile notification is-light ${this.notificationClass(indexClient)}`}>
                                    Client {indexClient} : {text}
                            </pre>;
        this.setState(prevState => ({
            logs: [textInHtml, ...prevState.logs]
        }))
    }

    clearLogs () {
        this.setState({
            logs: []
        })
    }

    notificationClass (index) {
        switch (index) {
            case "1":
                return "is-info";
            case "2":
                return "is-warning"
            case "3":
                return "is-success"
            case "4":
            default:
                return "is-link"
        }
    }

    render() {
        return (
            <div className="container">
                <section className="section">
                    <h1 className="title">Quatrième exercice - Le chat 🐈 (broadcast / diffusion)</h1>
                    <h2 className="subtitle">
                        Votre serveur doit diffuser à tous ses clients chaque message récéptionné.
                    </h2>
                    <div className="tile is-ancestor">
                        <Ex4Client index="1" notificationClass={this.notificationClass} logInHtml={this.logInHtml} />
                        <Ex4Client index="2" notificationClass={this.notificationClass} logInHtml={this.logInHtml} />
                    </div>
                    <div className="tile is-ancestor">
                        <Ex4Client index="3" notificationClass={this.notificationClass} logInHtml={this.logInHtml} />
                        <Ex4Client index="4" notificationClass={this.notificationClass} logInHtml={this.logInHtml} />
                    </div>
                </section>
                <section className="section">
                    <h2 className="title is-4">Logs&nbsp;&nbsp;<button className="button is-small" onClick={this.clearLogs}>Clear</button></h2>
                    <div className="box">
                        <div>{this.state.logs}</div>
                    </div>
                </section>
            </div>
            );
    }
}

export default Ex4;
import React from 'react';

class Ex4Client extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            // other stuff
            ws: null,
            wsOpen: false,
            message: '',
            logs: [],
            serverMessages: []
        };
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.createWebSocket = this.createWebSocket.bind(this);
        this.closeWebSocket = this.closeWebSocket.bind(this);
        this.clearServerMessages = this.clearServerMessages.bind(this);
        this.logInHtml = this.logInHtml.bind(this);
        this.messageInHtml = this.messageInHtml.bind(this);
        this.notificationClass = this.notificationClass.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentDidMount() {
        this.createWebSocket();
    }

    componentWillUnmount() {
        if (this.state.ws) {
            this.state.ws.onclose = null;
            this.closeWebSocket();
        }
    }

    logInHtml(text) {
        this.props.logInHtml(this.state.index, text);
    }

    messageInHtml(text) {
        const textInHtml = <pre style={{padding: "1px 5px", margin: "5px"}}>{text}</pre>;
        this.setState(prevState => ({
            serverMessages: [textInHtml, ...prevState.serverMessages]
        }))
    }

    createWebSocket () {
        const me = this;
        this.logInHtml('🧦 Tentative de connexion à la WebSocket');
        this.state.ws = new WebSocket('ws://localhost:8080/');

        this.state.ws.onerror = function() {
            me.logInHtml('❌ Erreur WebSocket');
        }

        this.state.ws.onmessage = function(e) {
            console.log("📨 Récéption d'un message du serveur : 📜 '" + e.data + "'");
            me.logInHtml("📨 Récéption d'un message du serveur : 📜 '" + e.data + "'");
            me.messageInHtml("📨📜 " + e.data);
        };

        this.state.ws.onclose = function (event) {
            me.setState({
                ws: null,
                wsOpen: false
            });
            if (event.code === 1000) {
                me.logInHtml(`${event.code} : Connexion WebSocket fermée avec succès`);
            } else {
                me.logInHtml(`${event.code} : Connexion WebSocket fermée suite à une erreur`);
                me.createWebSocket();
            }
        }

        this.state.ws.onopen = function() {
            console.log('🔌 Je suis connecté');
            me.logInHtml('🔌 Je suis connecté');
            me.setState({
                wsOpen: true,
            })
        };
    }
    
    closeWebSocket() {
        if (this.state.ws) {
            this.state.ws.close(1000);
        }
    }

    clearServerMessages () {
        this.setState({
            serverMessages: []
        })
    }

    notificationClass () {
        switch (this.state.index) {
            case "1":
                return "is-info";
            case "2":
                return "is-warning"
            case "3":
                return "is-success"
            case "4":
            default:
                return "is-link"
        }
    }

    handleMessageChange(event) {
        this.setState({
            message: event.target.value
        });
    }

    sendMessage () {
        if (this.state.message) {
            console.log(`🏌🏻💨✉️ Tentative d'envoi de message au serveur : '${this.state.message}'`);
            this.logInHtml(`🏌🏻💨✉️ Tentative d'envoi de message au serveur : '${this.state.message}'`);
            this.state.ws.send(this.state.message);
            this.setState({ message : ''});
        }
    }

    handleKeyPress (event) {
        if( event.key === 'Enter' ){
            this.sendMessage()
        }
    }

    render() {
        return (
            <div className="tile is-parent" onKeyPress={this.handleKeyPress}>
                <div className={`tile notification is-light ${this.props.notificationClass(this.state.index)}`}>
                    <article className="tile is-child is-4">
                        <p className="subtitle">Client {this.state.index} 🧦</p>
                        <div className="field">
                            <div className="control">
                                <input className="input is-normal" type="text" placeholder="Message" style={{width:"155px"}}
                                    value={this.state.message} onChange={this.handleMessageChange} />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button className="button is-normal" disabled={!this.state.message} onClick={this.sendMessage}>🏌🏻💨✉️ Envoyer</button>
                            </div>
                        </div>
                    </article>
                    <article className="tile is-child is-8">
                        <h1 style={{lineHeight: "30px", paddingBottom: "11px"}}>Messages du server&nbsp;&nbsp;&nbsp;<button className="button is-small" onClick={this.clearServerMessages}>Clear</button></h1>
                        <div className="box" style={{height: "100px", overflow: "hidden visible", padding: "5px"}}>
                            <div>{this.state.serverMessages}</div>
                        </div>
                    </article>
                </div>
            </div>
            );
    }
}

export default Ex4Client;
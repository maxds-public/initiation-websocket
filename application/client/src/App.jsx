import { useState } from 'react';
import './App.css';
import Ex1 from './components/Ex1';
import Ex2 from './components/Ex2';
import Ex3 from './components/Ex3/Ex3';
import Ex4 from './components/Ex4/Ex4';
import Ex5 from './components/Ex5/Ex5';
import Ex6 from './components/Ex6/Ex6';

function ExerciseDisplayer(props) {
  const selectedExercise = props.selectedExercise;
  if (selectedExercise === 'Exercice 2') {
    return <Ex2 />;
  } else if (selectedExercise === 'Exercice 3') {
    return <Ex3 />
  } else if (selectedExercise === 'Exercice 4') {
    return <Ex4 />
  } else if (selectedExercise === 'Exercice 5') {
    return <Ex5 />
  } else if (selectedExercise === 'Exercice 6') {
    return <Ex6 />
  }
  return <Ex1 />;
}

function App() {
  
  const exercises = ['Exercice 1', 'Exercice 2', 'Exercice 3', 'Exercice 4', 'Exercice 5', 'Exercice 6'];
  
  const [selectedExercise, setSelectedExercise] = useState('Exercice 1');

  const updateExercise = (ex) => {
    setSelectedExercise(ex);
  }

  return (
    <div className="App">
        <div className="name"><h1>Clank 🤖</h1></div>
        <div className="tabs is-centered">
            <ul>
            {exercises.map((ex, index) => <li
                    key={index}
                    className={ex === selectedExercise ? 'is-active' : null}
                    onClick = {updateExercise.bind(null, ex)}
                    ><a>{ex}</a></li> 
            )}
            </ul>
        </div>
        <ExerciseDisplayer selectedExercise={selectedExercise} />
    </div>
  );
}

export default App

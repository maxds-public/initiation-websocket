# Ex1

UNE POIGNÉE DE MAIN 🤝

- Créer un serveur permettant d'accepter une connexion WebSocket sur le port 8080.
- Vérifier qu'on reçoit bien les trames de connexion & déconnexion du client.

## Aides

```javascript
// Création d'un serveur WebSocket
const wss = new WebSocketServer({ port: numeroduport });

// l'objet wss a une fonction 'on' permettant d'écouter un évenement 'connection' :
wss.on('connection', function connection(ws) {
    // Ici est le code joué une fois la connexion établie
});

// l'objet ws à l'intéreur de la fonction `connection` à aussi une fonction 'on'
ws.on(EVENT, function() {
    // Ici est le code joué suite à un événement (EVENT) sur la websocket (ws)
    // EVENT peut prendre les valeurs suivantes :
    // 'open', 'close' ou 'message'
});

```

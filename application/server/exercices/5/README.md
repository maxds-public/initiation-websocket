# Ex5

ENVOYER DES OBJETS JSON

- Votre serveur doit diffuser à tous ses clients chaque message récéptionné mais cette fois en indiquant de quel client vient le message.
- Cela est possible grâce au format JSON 🙂.
- Le client va cette fois vous envoyer un message sous forme d'objet JSON comme suit :

```json
{
    "nom": "Jean-Michel", 
    "message": "Coucou tout le monde"
}
```

Dans cet exemple, vous devez alors construire le message suivant à renvoyer aux autres clients :
"Jean-michel : Coucou tout le monde"

## Aides

``` javascript
// L'objet data est en JSON, 
// Il faut donc le convertir en objet javascript pour pouvoir l'exploiter
const object = JSON.parse(data);

// Pour éviter tout problème si jamais le message 'data' n'est pas au format JSON
// Entourer le JSON.parse(data) et ce qui suit par un try catch
try {
    const object = JSON.parse(data);
    ...
} catch (e) {  
    console.log("Le message n'est pas au format json");
}

```

# Ex3

MULTI-CLIENTS 🎮

- Votre serveur doit diffuser un message à chacun de ses clients à chaque connexion et déconnexion.

## Aides

```javascript
const wss = new WebSocketServer({ port: numeroduport });

// l'objet wss a une liste où sont enregistrés automatiquement tous les clients :
wss.clients
// chaque élément de la liste est une websocket (comme ws vu dans les précédents TPs)
// elle a donc à sa disposition la fonction 'send'
client.send("")
// Avant d'envoyer un message à un client il est bon de s'assurer que celui-ci ait une connexion ouverte
// (en effet il pourrait être en cours de connexion ou de déconnexion)
// Pour ce faire, on peut connaître l'état du client via :
client.readyState
// L'état peut prendre les valeurs suivantes :
WebSocket.CONNECTING
WebSocket.OPEN // celle qui nous intéresse
WebSocket.CLOSING
WebSocket.CLOSED
```

# Ex2

RECEVOIR & ENVOYER DES MESSAGES ✉️

- Le client permet d'envoyer des messages, votre serveur doit être capable de les récéptionner et de renvoyer un message en retour.

## Aides

```javascript
const wss = new WebSocketServer({ port: numeroduport });

// l'objet wss a une fonction 'on' permettant d'écouter un évenement 'connection' :
wss.on('connection', function connection(ws) {
    // Ici est le code joué une fois la connexion établie
});

// l'objet ws à l'intéreur de la fonction `connection` a aussi une fonction 'on'
ws.on(EVENT, function() {
    // Ici est le code joué suite à un événement (EVENT) sur la websocket (ws)
    // EVENT peut prendre les valeurs suivantes :
    // 'open', 'close' ou 'message'
})

// ws a également une fonction 'send' permettant d'envoyer un message au client connecté
ws.send("coucou");
```

# Ex4

LE CHAT 🐈 (broadcast / diffusion)

- Votre serveur doit diffuser à tous ses clients chaque message récéptionné.
- Bonus : Ne pas renvoyer le message au destinataire

## Aides

``` javascript
// Le listener 'message' prend une fonction avec 2 paramètres : le message, et isBinary (un booléen)
// Le protocole Websocket permet d'envoyer 2 types de messages
// soit du texte, soit du binaire
// isBinary indique justement vrai si binaire, false sinon
ws.on('message', function message(message, isBinary) {
    ...
});

// en retour il faut alors bien indiquer également au client le type de message
client.send(message, { binary: isBinary });
```

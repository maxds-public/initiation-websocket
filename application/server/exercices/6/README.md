# Ex6

HEARTBEAT

Votre serveur doit s'assurer via un système de ping - pong que les clients sont toujours bien là.

L'idée est la suivante :

Le serveur va conserver un booléen `isAlive` sur chaque client.
Ce booléen est initialisé à `true` au moment de l'ouverture de la connexion.

Le serveur va aussi, toutes les 5 secondes, parcourir l'ensemble de ses clients et pour chacun d'eux :

- Regarder s'il est bien "en vie' : Si `isAlive` est à `false`, dans ce cas mettre fin à la connexion (`ws.terminate()`)
- S'il est bien "en vie", mettre `isAlive` à `false` puis lui envoyer un message `ping`.

Le client, à chaque fois qu'il reçoit `ping`, renvoie à son tour `pong`.

Si le serveur reçoit `pong`, alors il remet `isAlive` à `true`.

## Aides

``` javascript
// Afin de lancer une action toutes les 5 secondes,
// on peut utiliser en js la methode `setInterval(function)` comme suit:
const pingClientsInterval = setInterval(function () {
    // LE CODE ICI S'EXECUTE TOUTES LES 5000 MS
}, 5000);

// Penser à faire un toString() sur les messages reçu afin de le comparer à "pong"
message.toString()

// Ne pas oublier d'arrêter le timer sur l'evenement 'close'.
wss.on('close', function close() {
  clearInterval(pingClientsInterval);
});
```

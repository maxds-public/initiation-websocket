import WebSocket, { WebSocketServer } from 'ws';

const wss = new WebSocketServer({ port: 8080 });

wss.on('connection', function connection(ws) {
    console.log('Client connecté');
    
    ws.on('message', function message(data, isBinary) {
        try {
            console.log('Message reçu : ', JSON.parse(data));
            data = JSON.parse(data);
            const messageRetour = data.nom + ' : ' + data.message;
            wss.clients.forEach(function each(client) {
                if (client.readyState === WebSocket.OPEN) {
                    if (client !== ws) {
                        client.send(messageRetour, { binary: isBinary });
                    }
                }
            }); 
        } catch (e) {  
            console.log("Le message n'est pas au format json");  
        }
    });
});
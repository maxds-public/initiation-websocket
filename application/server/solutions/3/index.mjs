import WebSocket, { WebSocketServer } from 'ws';

const wss = new WebSocketServer({ port: 8080 });

wss.on('connection', function connection(ws) {
    console.log('Client connecté');

    // envoi d'un message à tous les clients lors d'une connexion
    wss.clients.forEach(function each(client) {
        // on s'assure que le client à une connexion ouverte 
        // (il pourrait être en cours de connexion ou de fermeture)
        if (client.readyState === WebSocket.OPEN) {
            client.send('Un client vient de se connecter');
        }
    });

    ws.on('close', function close() {
        console.log('Client déconnecté');
        // envoi d'un message à tous les clients lors d'une connexion
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
                client.send('Un client vient de se déconnecter');
            }
        });
    });

});
import WebSocket, { WebSocketServer } from 'ws';

const wss = new WebSocketServer({ port: 8080 });

wss.on('connection', function connection(ws) {
    console.log('Client connecté');
    
    ws.on('message', function message(data, isBinary) {
        console.log('Message reçu : %s', data);
        wss.clients.forEach(function each(client) {
            // étape bonus (client !== ws)
            // on envoi le message à tout le monde sauf nous même.
            if (client.readyState === WebSocket.OPEN && client !== ws) {
                // Attention au isBinary
                // Pour du texte : binary va être à false
                // Binary sera à vrai sinon
                client.send(data, { binary: isBinary });
            }
        });
    });

});
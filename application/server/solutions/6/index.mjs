import { WebSocketServer } from 'ws';

const wss = new WebSocketServer({ port: 8080 });

wss.on('connection', function connection(ws) {
    ws.isAlive = true;

    ws.on('message', function message(message) {
        console.log('Message reçu, ', message.toString());
        if (message.toString() === "pong") {
            console.log("pong reçu");
            ws.isAlive = true;
        }
    });
});

const pingClientsInterval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();

    ws.isAlive = false;
    ws.send("ping");
    console.log("ping envoyé");
  });
}, 5000);

wss.on('close', function close() {
  clearInterval(pingClientsInterval);
});
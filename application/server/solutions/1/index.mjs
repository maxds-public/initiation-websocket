import { WebSocketServer } from 'ws';

const wss = new WebSocketServer({ port: 8080 });

wss.on('connection', function connection(ws) {
    console.log('Client connecté');

    ws.on('close', function close() {
        console.log('Client déconnecté');
    });
});
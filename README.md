# initiation-websocket

Projet dédié à la formation Websocket 🧦.

## Mise en place

### Installation et démarrage du client

```bash
cd application/client

npm install

npm run start
```

### Installation du serveur

```bash
cd ../server

npm install
```

## C'est parti pour les exercices !
### Installation du package node "ws"

```bash
npm install ws
```

### Lancement de l'exercice 1 (ex1)
```bash
npm run ex1
```

## C'est le moment de CODER

Tu es fin prêt, c'est maintenant le moment de coder 🐱‍💻 !

Retrouve les instructions de chaque exercice dans les dossiers application/server/exercices/numerodelexercice